const router = require('express').Router()
const Note = require('../models/Note')
const {isAuthenticated} = require('../helpers/auth')

router.get('/notes/add', isAuthenticated, (req, res) => {res.render('notes/new-note')})

router.post('/notes/new-note', isAuthenticated, async (req,res) => {
    const {title,description} = req.body;
    const errors = []
    if(!title) {errors.push('Input a valid title')}
    if (!description) { errors.push('Input a valid description') }
    if(errors.length>0)
    {
        res.render('notes/new-note',{errors,title,description})
    }
    else
    {
        const note = new Note({title,description})
        note.user = req.user.id
        await note.save()
        req.flash('success_msg','Note added')
        res.redirect('/notes')
    }
})

router.get('/notes', isAuthenticated, async (req, res) => {
    const notes = await Note.find({user: req.user.id}).sort({date: 'desc'})
    const count = notes.length
    const newArray = {
        items: notes.map(n=>{
            return {
                title: n.title,
                description: n.description,
                id: n._id
            }
        })
    }
    res.render('notes/all-notes',{notes: newArray.items,count})
})

router.get('/notes/edit/:id', isAuthenticated, async (req,res) => {
    const note = await Note.findById(req.params.id)
    const {id,title,description} = note
    res.render('notes/edit-note', { id, title, description })
})

router.put('/notes/edit-note/:id', isAuthenticated, async (req,res) => {
    const {title,description} = req.body
    await Note.findByIdAndUpdate(req.params.id,{title,description})
    req.flash('success_msg', 'Note updated successfully')
    res.redirect('/notes')
})

router.delete('/notes/delete/:id', isAuthenticated, async (req,res) => {
    await Note.findByIdAndDelete(req.params.id)
    req.flash('success_msg', 'Note deleted successfully')
    res.redirect('/notes')
})
module.exports = router;