const router = require('express').Router()
const User = require('../models/User')
const passport = require('passport')

router.get('/users/signin', (req, res) => {res.render('users/signin') })
router.get('/users/signup', (req, res) => {res.render('users/signup') })
router.post('/users/signup',async (req,res) => {
    const { name, email, password, confirm_password} = req.body
    const errors = []
    if(name.length<=0)
    {
        errors.push('Empty name not allowed')
    }
    if(password!==confirm_password)
    {
        errors.push('Passwords do not match')
    } 
    if(password.length<=4)
    {
        errors.push('Your password is too short (at least 6 characters)')
    }
    if(errors.length>0)
    {
        res.render('users/signup',{errors,name,email})
    }
    else
    {
        const existingUser = await User.findOne({email: email})
        if(existingUser)
        {
            req.flash('error_msg','Email already registered, try with other')
            res.redirect('/users/signup')
            return
        }
        const user = new User({name,email,password})
        user.password = await user.encryptPassword(password)
        await user.save()
        req.flash('success_msg','User registered')
        res.redirect('/users/signin')
    }
})
router.post('/users/signin',passport.authenticate('local',{
    successRedirect: '/notes',
    failureRedirect: '/users/signin',
    failureFlash: true
}))

router.get('/users/logout',(req,res) => {
    req.logout()
    res.redirect('/')
})
module.exports = router;