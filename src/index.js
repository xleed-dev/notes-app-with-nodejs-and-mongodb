// Module imports
const express = require('express')
const path = require('path')
const expHbs = require('express-handlebars')
const methodOverride = require('method-override')
const session = require('express-session')
const flash =  require('connect-flash')
const passport = require('passport')


// Server initialization
const app = express()
require('./database')
require('./config/passport')

// Settings
app.set('port',process.env.port | 3000)
app.set('views',path.join(__dirname,'views'))
app.engine('.hbs',expHbs({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'),'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs'
}))
app.set('view engine', '.hbs')


// Middlewares
app.use(express.urlencoded({extended: false}))
app.use(methodOverride('_method'))
app.use(session({
    secret: 'mysecretapp',
    resave: true,
    saveUninitialized: true
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(flash())

// Global Vars

// add middleware
app.use((req,res,next) => {
    res.locals.success_msg = req.flash('success_msg')
    res.locals.error_msg = req.flash('error_msg')
    res.locals.error = req.flash('error')
    var userCopy = null
    if(req.user!==undefined)
    {
        userCopy = {
            name: req.user.name,
            email: req.user.email
        }
    }
    res.locals.user = userCopy
    next()
})

// Routes
app.use(require('./routes/index'))
app.use(require('./routes/notes'))
app.use(require('./routes/users'))

// Static files
app.use(express.static(path.join(__dirname,'public')))

// Server running
app.listen(app.get('port'), () => {
    console.log("Server running on port:", app.get('port'));
})