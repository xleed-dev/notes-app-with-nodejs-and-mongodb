const mongoose = require('mongoose')
const { Schema } = mongoose
const bcrypt  = require('bcryptjs')

const userSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    date: { type: Date, default: Date.now },
})

userSchema.methods.encryptPassword = async (pass) => {
    const salt = await bcrypt.genSalt(10)
    const hash = bcrypt.hash(pass,salt)
    return hash
}

userSchema.methods.matchPassword = async function(pass) {
    return await bcrypt.compare(pass,this.password)
}

module.exports = mongoose.model('User', userSchema)